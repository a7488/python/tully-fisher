import requests
import webbrowser
from bs4 import BeautifulSoup
from matplotlib import pyplot as plt
import cmath 


par = {'d':'objname,bt,vrot,modbest', 'sql':'bt<14 and vrot IS NOT NULL and (incl>60)','a':'csv' }
link = "http://leda.univ-lyon1.fr/fG.cgi?n=meandata&c=o&of=1,leda,simbad&nra=l&nakd=1"
response = requests.get(link, params = par)
webbrowser.open(response.url)
soup = BeautifulSoup(response.text, 'html.parser')
file = open("hyper_leda.csv",'w')
file.write(str(soup))

file1 = open("hyper_leda1.csv",'w')
with open("hyper_leda.csv",'r') as file:
    for line in file:
        if "#"  in line:
            continue
        file1.write(str(line))

file1 = open("hyper_leda1.csv",'r')
lines = file1.readlines()


obj_names = []
bt = []
vrot = []
modbest = []
abs_magn = []
log10_vrot = []


for i in range(1, len(lines)-1):
        obj_names.append((lines[i].split())[0])
        bt.append((lines[i].split())[1])
        vrot.append((lines[i].split())[2])
        modbest.append((lines[i].split())[3])
        abs_magn.append( float((lines[i].split())[1]) - float((lines[i].split())[3])) 
        log10_vrot.append(cmath.log10(float(((lines[i].split())[2]))))

#print(max(log10_vrot), min(log10_vrot))

plt.title("Tully–Fisher relation")
plt.ylabel("$\log_{10}{v_{r}}$")
plt.xlabel("$m - \mu$")
plt.grid()
plt.plot(abs_magn, log10_vrot, 'ro', markersize=3)
plt.show()




